// Module dependencies.
var express = require("express")
, http    = require("http")
, path    = require("path")
, routes  = require("./routes")
, breadcrumb = require('express-url-breadcrumb');
var app     = express();

// All environments
app.set("port", 3000);
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use(express.favicon());
app.use(express.logger("dev"));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser("61d333a8-6325-4506-96e7-a180035cc26f"));
app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, "public")));
app.use(express.errorHandler());
app.use(breadcrumb());



// App routes
app.get("/shop/", routes.index);
app.get('/shop/:category', routes.shop);
app.get('/shop/:category/:subcategory',routes.subcategory);
app.get('/shop/:category/:subcategory/:productsroute',routes.products);
app.get('/shop/:category/:subcategory/:productsroute/:productid',routes.product);
// app.get("/shop/Mens", routes.categoryMen);
// app.get("/shop/Womens", routes.categoryWomen);

// Run server
http.createServer(app).listen(app.get("port"), function() {
console.log("Express server listening on port " + app.get("port"));
});
