exports.index = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	
	mdbClient.connect("mongodb://localhost:27017/myDb", function(err, db) {
		var collection = db.collection('categories');
		
		collection.find().toArray(function(err, items) {
			res.render("index", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				
				title : "shop World!",
				page_category : 'index',
				items : items
			});

			db.close();
		});
	});
};

exports.categoryMen = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	
	mdbClient.connect("mongodb://localhost:27017/myDb", function(err, db) {
		var collection = db.collection('categories');
		
		collection.find().toArray(function(err, items) {
			res.render("categoryMen", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				
				title : "shop World!",
				page_category : 'categoryMen',
				items : items
			});

			db.close();
		});
	});
};


exports.categoryWomen = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	
	mdbClient.connect("mongodb://localhost:27017/myDb", function(err, db) {
		var collection = db.collection('categories');
		
		collection.find().toArray(function(err, items) {
			res.render("categoryWomen", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				
				title : "shop World!",
				page_category : 'categoryWomen',
				items : items
			});

			db.close();
		});
	});
};


exports.shop = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	
	mdbClient.connect("mongodb://localhost:27017/myDb", function(err, db) {
		var collection = db.collection('categories');
		
		collection.find().toArray(function(err, items) {
			res.render("shop", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				category: req.params.category,
				title : "shop World!",
				page_category : 'shop',
				items : items
			});

			db.close();
		});
	});
};

exports.subcategory = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	
	mdbClient.connect("mongodb://localhost:27017/myDb", function(err, db) {
		var collection = db.collection('categories');
		
		collection.find().toArray(function(err, items) {
			res.render("subcategory", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				category: req.params.category,
				subcategory: req.params.subcategory,
				title : "subcategory",
				page_category : 'subcategory',
				items : items
			});

			db.close();
		});
	});
};

exports.products = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	
	mdbClient.connect("mongodb://localhost:27017/myDb", function(err, db) {
		var collection = db.collection('products');
		
		collection.find().toArray(function(err, items) {
			res.render("products", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				category: req.params.category,
				subcategory: req.params.subcategory,
				productsroute: req.params.productsroute,
				title : "products",
				page_category : 'products',
				items : items
			});

			db.close();
		});
	});
};
exports.product = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	
	mdbClient.connect("mongodb://localhost:27017/myDb", function(err, db) {
		var collection = db.collection('products');
		
		collection.find().toArray(function(err, items) {
			res.render("product", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				category: req.params.category,
				subcategory: req.params.subcategory,
				productsroute: req.params.productsroute,
				productid: req.params.productid,
				title : "product",
				page_category : 'product',
				items : items
			});

			db.close();
		});
	});
};


