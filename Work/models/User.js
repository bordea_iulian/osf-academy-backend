var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var Schema = mongoose.Schema;
mongoose.connect('mongodb://localhost:27017/myDb' );
var db = mongoose.connection;

var userSchema = new Schema({
    name: {type: String, required:true},
    email: { type: String, unique: true, required:true},
    password:{ type:  String ,required:true},
    address:{ type:  String ,required:true},
  });

 var User = mongoose.model('users', userSchema);
 module.exports = User;

